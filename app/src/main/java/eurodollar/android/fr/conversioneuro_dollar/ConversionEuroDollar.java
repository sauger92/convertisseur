package eurodollar.android.fr.conversioneuro_dollar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ConversionEuroDollar extends AppCompatActivity {
    int boutonCree ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_euro_dollar);
        boutonCree =0;



    }


    public void myClickHandler(View view) {
        EditText Text = (EditText) findViewById(R.id.editText1);
        LinearLayout ll = (LinearLayout)findViewById(R.id.layout);

        Button btn = new Button(getBaseContext());
        btn.setText("Quitter");
        btn.setId(R.id.btn);
        switch (view.getId()) {

            case R.id.button1:
                RadioButton euroButton = (RadioButton) findViewById(R.id.radio0);
                RadioButton dollarButton = (RadioButton) findViewById(R.id.radio1);
                if (boutonCree==0)
                {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                   ll.addView(btn, layoutParams);
                    boutonCree=1;
                }
                if (Text.getText().length() == 0) {
                    Toast.makeText(this, "Please enter a valid number",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                float inputValue = Float.parseFloat(Text.getText().toString());
                if (euroButton.isChecked()) {
                    Text.setText(String
                            .valueOf(convertDollarToEuro(inputValue)));
                    euroButton.setChecked(false);
                    dollarButton.setChecked(true);
                } else {
                    Text.setText(String
                            .valueOf(convertEuroToDollar(inputValue)));
                    dollarButton.setChecked(false);
                    euroButton.setChecked(true);


                }
                break;

            case R.id.btn:
            finish();
                break;


        }
    }

    // Convertir Dollar à Euro
    private float convertDollarToEuro(float dollar) {
        return (float) dollar/(float)1.1939; // formule à utiliser
    }

    // Convertir Euro à Dollar
    private float convertEuroToDollar(float euro) {
        return (float) euro*(float)1.1939; // formule à utiliser
    }
}


